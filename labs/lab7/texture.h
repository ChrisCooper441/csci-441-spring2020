//
// Created by Chris on 3/8/2020.
//

#ifndef LAB7_TEXTURE_H
#define LAB7_TEXTURE_H
#pragma once

#include <stb/stb_image.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Texture
{
public:
    Texture();
    Texture(char* fileLoc);

    void LoadTexture();
    void UseTexture();
    void ClearTexture();

    ~Texture();

private:
    GLuint textureID;
    int width, height, bitDepth;

    char* fileLocation;
};


#endif //LAB7_TEXTURE_H
