#version 330 core

uniform vec3 light;
uniform vec3 viewPos;

in vec3 ourColor;
in vec3 ourNormal;
in vec3 ourPos;

out vec4 fragColor;

void main() {
    vec3 lightDir = normalize(light - ourPos);
    vec3 norm = normalize(ourNormal);
    vec3 lightColor = vec3(1.0, 1.0, 1.0);

    // https://learnopengl.com/Lighting/Basic-Lighting
    float ambientStrength = 0.3;
    float spectralStrength = 0.5;

    vec3 ambient = ambientStrength * lightColor;
    vec3 diffuse = max(dot(ourNormal, lightDir), 0.0) * lightColor;
    vec3 viewDir = normalize(viewPos - ourPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 spectral = spectralStrength * spec * lightColor;

    vec3 finalColor = (ambient + diffuse + spectral) * ourColor;
    fragColor = vec4(finalColor, 1.0f);
}
