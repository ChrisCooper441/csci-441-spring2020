//
// Chris Cooper CSCI 441
//

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "Matrix4.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
float norm_x=0;
float norm_y=0;
float norm_z=0;
float view_x=0;
float view_y=0;
float view_z=0;
float object_size=1;
int mode_action=0;
const int view_max = 20;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    // Part 1 - View Matrix (
    else if(glfwGetKey(window, GLFW_KEY_W)==GLFW_PRESS){
        if(object_size<view_max) {
            object_size = object_size + .01;
        }
    }
    else if(glfwGetKey(window, GLFW_KEY_S)==GLFW_PRESS){
        if(object_size>-view_max) {
            object_size = object_size - .01;
        }
    }
    // Part 2 - Model Matrix
    else if(glfwGetKey(window, GLFW_KEY_UP)==GLFW_PRESS){
        norm_y=norm_y+.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_DOWN)==GLFW_PRESS){
        norm_y= norm_y-.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_LEFT)==GLFW_PRESS){
        norm_x=norm_x-.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_RIGHT)==GLFW_PRESS){
        norm_x=norm_x+.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_COMMA)==GLFW_PRESS){
        norm_z=norm_z-.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_PERIOD)==GLFW_PRESS){
        norm_z=norm_z+.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_MINUS)==GLFW_PRESS){
        object_size=(object_size*.99);
    }
    else if(glfwGetKey(window, GLFW_KEY_EQUAL)==GLFW_PRESS){
        object_size=(object_size*1.01);
    }
    // use u, i, to increase and decrease rotation around x-axis
    else if(glfwGetKey(window, GLFW_KEY_U)==GLFW_PRESS){
        view_x=view_x+.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_I)==GLFW_PRESS){
        view_x=view_x-.01;
    }
    //o and p to rotate around y-axis
    else if(glfwGetKey(window, GLFW_KEY_O)==GLFW_PRESS){
        view_y=view_y+.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_P)==GLFW_PRESS){
        view_y=view_y-.01;
    }
    // [ and ] to rotate around the z-axis
    else if(glfwGetKey(window, GLFW_KEY_LEFT_BRACKET)==GLFW_PRESS){
        view_z=view_z+.01;
    }
    else if(glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET)==GLFW_PRESS){
        view_z=view_z-.01;
    }

    // Use the \ to switch between projection types.
    else if((mode_action == 0) && glfwGetKey(window, GLFW_KEY_BACKSLASH)==GLFW_PRESS){
        mode_action = 1;         // perspective
    }
    else if((mode_action == 1) && glfwGetKey(window, GLFW_KEY_BACKSLASH)==GLFW_PRESS){
        mode_action = 0;        // orthogonal
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the model */
    float vertices[] = {
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
            0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
            0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
            0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
            0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
            0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
            0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };


    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
                          (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
                          (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);
//    Matrix4 drawData;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {

        Matrix4 x_val = rotate(view_y, 'x');
        Matrix4 y_val = rotate(view_y, 'y');
        Matrix4 z_val = rotate(view_z, 'z');
        Matrix4 translated = translate(norm_x, norm_y, norm_z);
        Matrix4 scaled = scale(50);
        Matrix4 ortho = orthogonal(1.0, -1.0, -1.0, 1.0, .1, -100);


        // set camera
        Matrix4 view_matrix = viewport(object_size/10);
//        view_matrix.set_7(object_size);
//        view_matrix.set_11(20);

//        view_matrix.print();
        Matrix4 transform = scaled * z_val * x_val * y_val * translated;

        // create arrays to hold values
        float proj_array[16];
        float view_array[16];
        float mod_array[16];
        toArray(mod_array, transform);
        toArray(view_array, view_matrix);
        toArray(proj_array, ortho);

        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        unsigned int transformLocation = glGetUniformLocation(shader.id(), "transform");
        unsigned int projectionLocation = glGetUniformLocation(shader.id(), "project");
        unsigned int viewportLocation = glGetUniformLocation(shader.id(), "viewport");
        glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, proj_array);
        glUniformMatrix4fv(transformLocation, 1, GL_FALSE, mod_array);
        glUniformMatrix4fv(viewportLocation, 1, GL_FALSE, view_array);


        // activate shader
        shader.use();

        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
