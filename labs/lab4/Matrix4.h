//
// Matrix file created for better manipulation of matrices
// Collaborated with Jarey Boyer
// Extension of Matrix3 from lab3

#ifndef LAB4_MATRIX3_H
#define LAB4_MATRIX3_H

#endif //LAB4_MATRIX3_H
#include <cmath>
#include <iostream>
#include <GLFW/glfw3.h>

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <array>


class Matrix4 {
public:
    float data[16];

    // default fills with identity
    Matrix4(){
        data[0] = 1;
        data[1] = 0;
        data[2] = 0;
        data[3] = 0;
        data[4] = 0;
        data[5] = 1;
        data[6] = 0;
        data[7] = 0;
        data[8] = 0;
        data[9] = 0;
        data[10] = 1;
        data[11] = 0;
        data[12] = 0;
        data[13] = 0;
        data[14] = 0;
        data[15] = 1;


    }

    Matrix4(float inputData[16]){
        data[0] = inputData[0];
        data[1] = inputData[1];
        data[2] = inputData[2];
        data[3] = inputData[3];
        data[4] = inputData[4];
        data[5] = inputData[5];
        data[6] = inputData[6];
        data[7] = inputData[7];
        data[8] = inputData[8];
        data[9] = inputData[9];
        data[10] = inputData[10];
        data[11] = inputData[11];
        data[12] = inputData[12];
        data[13] = inputData[13];
        data[14] = inputData[14];
        data[15] = inputData[15];

    }

    // getters
    float get_0() const {return data[0];}
    float get_1() const {return data[1];}
    float get_2() const {return data[2];}
    float get_3() const {return data[3];}
    float get_4() const {return data[4];}
    float get_5() const {return data[5];}
    float get_6() const {return data[6];}
    float get_7() const {return data[7];}
    float get_8() const {return data[8];}
    float get_9() const {return data[9];}
    float get_10() const {return data[10];}
    float get_11() const {return data[11];}
    float get_12() const {return data[12];}
    float get_13() const {return data[13];}
    float get_14() const {return data[14];}
    float get_15() const {return data[15];}

    // setters
    void set_0(float f){data[0]=f;}
    void set_1(float f){data[1]=f;}
    void set_2(float f){data[2]=f;}
    void set_3(float f){data[3]=f;}
    void set_4(float f){data[4]=f;}
    void set_5(float f){data[5]=f;}
    void set_6(float f){data[6]=f;}
    void set_7(float f){data[7]=f;}
    void set_8(float f){data[8]=f;}
    void set_9(float f){data[9]=f;}
    void set_10(float f){data[10]=f;}
    void set_11(float f){data[11]=f;}
    void set_12(float f){data[12]=f;}
    void set_13(float f){data[13]=f;}
    void set_14(float f){data[14]=f;}
    void set_15(float f){data[15]=f;}

    void print() const {
        std::cout << "["<< data[0] << "," << data[1] << "," << data[2] << "," << data[3] << std::endl;
        std::cout << data[4] << "," << data[5] << "," << data[6] << ","<< data[7] << std::endl;
        std::cout << data[8] << "," << data[9] << "," << data[10] << ","<< data[11] << std::endl;
        std::cout << data[12] << "," << data[13] << "," << data[14] << ","<< data[15] << "]" << std::endl;
    }
};

void toArray(float *inputArray, Matrix4 inputMatrix) {
    inputArray[0] = inputMatrix.get_0();
    inputArray[1] = inputMatrix.get_1();
    inputArray[2] = inputMatrix.get_2();
    inputArray[3] = inputMatrix.get_3();
    inputArray[4] = inputMatrix.get_4();
    inputArray[5] = inputMatrix.get_5();
    inputArray[6] = inputMatrix.get_6();
    inputArray[7] = inputMatrix.get_7();
    inputArray[8] = inputMatrix.get_8();
    inputArray[9] = inputMatrix.get_9();
    inputArray[10] =inputMatrix.get_10();
    inputArray[11] =inputMatrix.get_11();
    inputArray[12] =inputMatrix.get_12();
    inputArray[13] =inputMatrix.get_13();
    inputArray[14] =inputMatrix.get_14();
    inputArray[15] =inputMatrix.get_15();
}

Matrix4 operator *(const Matrix4 &a, const Matrix4 &b){
    // https://www.khanacademy.org/math/precalculus/x9e81a4f98389efdf:matrices/x9e81a4f98389efdf:properties-of-matrix-multiplication/a/matrix-multiplication-dimensions
    Matrix4 output;
    output.set_0(a.get_0()*b.get_0()+a.get_4()*b.get_1()+a.get_8()*b.get_2()+a.get_12()*b.get_3());
    output.set_1(a.get_1()*b.get_0()+a.get_5()*b.get_1()+a.get_9()*b.get_2()+a.get_13()*b.get_3());
    output.set_2(a.get_2()*b.get_0()+a.get_6()*b.get_1()+a.get_10()*b.get_2()+a.get_14()*b.get_3());
    output.set_3(a.get_3()*b.get_0()+a.get_7()*b.get_1()+a.get_11()*b.get_2()+a.get_15()*b.get_3());
    output.set_4(a.get_0()*b.get_4()+a.get_4()*b.get_5()+a.get_8()*b.get_6()+a.get_12()*b.get_7());
    output.set_5(a.get_1()*b.get_4()+a.get_5()*b.get_5()+a.get_9()*b.get_6()+a.get_13()*b.get_7());
    output.set_6(a.get_2()*b.get_4()+a.get_6()*b.get_5()+a.get_10()*b.get_6()+a.get_14()*b.get_7());
    output.set_7(a.get_3()*b.get_4()+a.get_7()*b.get_5()+a.get_11()*b.get_6()+a.get_15()*b.get_7());
    output.set_8(a.get_0()*b.get_8()+a.get_4()*b.get_9()+a.get_8()*b.get_10()+a.get_12()*b.get_11());
    output.set_9(a.get_1()*b.get_8()+a.get_5()*b.get_9()+a.get_9()*b.get_10()+a.get_13()*b.get_11());
    output.set_10(a.get_2()*b.get_8()+a.get_6()*b.get_9()+a.get_10()*b.get_10()+a.get_14()*b.get_11());
    output.set_11(a.get_3()*b.get_8()+a.get_7()*b.get_9()+a.get_11()*b.get_10()+a.get_15()*b.get_11());
    output.set_12(a.get_0()*b.get_12()+a.get_4()*b.get_13()+a.get_8()*b.get_14()+a.get_12()*b.get_15());
    output.set_13(a.get_1()*b.get_12()+a.get_5()*b.get_13()+a.get_9()*b.get_14()+a.get_13()*b.get_15());
    output.set_14(a.get_2()*b.get_12()+a.get_6()*b.get_13()+a.get_10()*b.get_14()+a.get_14()*b.get_15());
    output.set_15(a.get_3()*b.get_12()+a.get_7()*b.get_13()+a.get_11()*b.get_14()+a.get_15()*b.get_15());

    return output;
}

Matrix4 identity(){
    /*
       1 0 0 0
       0 1 0 0
       0 0 1 0
       0 0 0 1
     */
    Matrix4 identity;
    identity.set_0(1);
    identity.set_1(0);
    identity.set_2(0);
    identity.set_3(0);
    identity.set_4(0);
    identity.set_5(1);
    identity.set_6(0);
    identity.set_7(0);
    identity.set_8(0);
    identity.set_9(0);
    identity.set_10(1);
    identity.set_11(0);
    identity.set_12(0);
    identity.set_13(0);
    identity.set_14(0);
    identity.set_15(1);

    return identity;
}


Matrix4 rotate(float theta, char axis) {
    // separate into individual axes for x, y, and z
    Matrix4 rotationMatrix;
    if(axis == 'x'){
        rotationMatrix.set_0(1);
        rotationMatrix.set_1(0);
        rotationMatrix.set_2(0);
        rotationMatrix.set_3(0);
        rotationMatrix.set_4(0);
        rotationMatrix.set_5(cos(theta));
        rotationMatrix.set_6(sin(theta));
        rotationMatrix.set_7(0);
        rotationMatrix.set_8(0);
        rotationMatrix.set_9(-sin(theta));
        rotationMatrix.set_10(cos(theta));
        rotationMatrix.set_11(0);
        rotationMatrix.set_12(0);
        rotationMatrix.set_13(0);
        rotationMatrix.set_14(0);
        rotationMatrix.set_15(1);
    }

    if(axis == 'y'){
        rotationMatrix.set_0(cos(theta));
        rotationMatrix.set_1(0);
        rotationMatrix.set_2(sin(theta));
        rotationMatrix.set_3(0);
        rotationMatrix.set_4(0);
        rotationMatrix.set_5(1);
        rotationMatrix.set_6(0);
        rotationMatrix.set_7(0);
        rotationMatrix.set_8(-sin(theta));
        rotationMatrix.set_9(0);
        rotationMatrix.set_10(cos(theta));
        rotationMatrix.set_11(0);
        rotationMatrix.set_12(0);
        rotationMatrix.set_13(0);
        rotationMatrix.set_14(0);
        rotationMatrix.set_15(1);
    }

    if(axis == 'z'){
        rotationMatrix.set_0(cos(theta));
        rotationMatrix.set_1(sin(theta));
        rotationMatrix.set_2(0);
        rotationMatrix.set_3(0);
        rotationMatrix.set_4(-sin(theta));
        rotationMatrix.set_5(cos(theta));
        rotationMatrix.set_6(0);
        rotationMatrix.set_7(0);
        rotationMatrix.set_8(0);
        rotationMatrix.set_9(0);
        rotationMatrix.set_10(1);
        rotationMatrix.set_11(0);
        rotationMatrix.set_12(0);
        rotationMatrix.set_13(0);
        rotationMatrix.set_14(0);
        rotationMatrix.set_15(1);
    }

    return rotationMatrix;
}

Matrix4 scale(float scalar) {
    Matrix4 scaleMatrix;
    scaleMatrix.set_0(scalar / 100);
    scaleMatrix.set_1(0);
    scaleMatrix.set_2(0);
    scaleMatrix.set_3(0);
    scaleMatrix.set_4(0);
    scaleMatrix.set_5(scalar / 100);
    scaleMatrix.set_6(0);
    scaleMatrix.set_7(0);
    scaleMatrix.set_8(0);
    scaleMatrix.set_9(0);
    scaleMatrix.set_10(scalar / 100);
    scaleMatrix.set_11(0);
    scaleMatrix.set_12(0);
    scaleMatrix.set_13(0);
    scaleMatrix.set_14(0);
    scaleMatrix.set_15(1);

    return scaleMatrix;
}

Matrix4 translate(float x, float y, float z) {
    Matrix4 transMatrix;
    transMatrix.set_0(1);
    transMatrix.set_1(0);
    transMatrix.set_2(0);
    transMatrix.set_3(0);
    transMatrix.set_4(0);
    transMatrix.set_5(1);
    transMatrix.set_6(0);
    transMatrix.set_7(0);
    transMatrix.set_8(0);
    transMatrix.set_9(0);
    transMatrix.set_10(1);
    transMatrix.set_11(0);
    transMatrix.set_12(x);
    transMatrix.set_13(y);
    transMatrix.set_14(z);
    transMatrix.set_15(1);

    return  transMatrix;
}

Matrix4 orthogonal(const float l, const float r, const float b, const float t, const float n, const float f){
    Matrix4 orthoMatrix;
    /*
      2/(r-l)           0           0           0
      0                 2/(t-b)     0           0
      0                 0           2/(n-f)     0
      -(r+l)/(r-l)  -(t+b)/(t-b)  -(f+n)/(n-f)  1
     */

    orthoMatrix.set_0(2/(r-l));
    orthoMatrix.set_1(0);
    orthoMatrix.set_2(0);
    orthoMatrix.set_3(0);
    orthoMatrix.set_4(0);
    orthoMatrix.set_5(2/(t-b));
    orthoMatrix.set_6(0);
    orthoMatrix.set_7(0);
    orthoMatrix.set_8(0);
    orthoMatrix.set_9(0);
    orthoMatrix.set_10(2/(n-f));
    orthoMatrix.set_11(0);
    orthoMatrix.set_12(-(r+l)/(r-l));
    orthoMatrix.set_13(-(t+b)/(t-b));
    orthoMatrix.set_14(-(f+n)/(n-f));
    orthoMatrix.set_15(1);

    return orthoMatrix;
}

Matrix4 perspective(const float n, const float f){
    Matrix4 perspMatrix;
    perspMatrix.set_0(n);
    perspMatrix.set_1(0);
    perspMatrix.set_2(0);
    perspMatrix.set_3(0);
    perspMatrix.set_4(0);
    perspMatrix.set_5(n);
    perspMatrix.set_6(0);
    perspMatrix.set_7(0);
    perspMatrix.set_8(0);
    perspMatrix.set_9(0);
    perspMatrix.set_10(f+n);
    perspMatrix.set_11(1);
    perspMatrix.set_12(0);
    perspMatrix.set_13(0);
    perspMatrix.set_14(-n*f);
    perspMatrix.set_15(0);

    return perspMatrix;
}


Matrix4 viewport(float input){
    Matrix4 view, e, r;
    float h = input;
    int yd=20;
    float g=sqrt(h*h+yd*yd);
    e.set_0(-1);
    e.set_5(yd/g);
    e.set_6(-h/g);
    e.set_9(h/g);
    e.set_10(yd/g);
    e.set_15(1);

    r.set_13(-h);
    r.set_14(-yd);
    r.set_15(1);

    view = e*r;
    return view;
}



// reused Vector3 from lab1
class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx=0, float yy=0, float zz=0){ x=xx; y=yy; z=zz;
        // nothing to do here as we've already initialized x, y, and z above
        //std::cout << "in Vector3 constructor" << std::endl;
    }
    float xval() const {return x;}
    float yval() const {return y;}
    float zval() const {return z;}


    void set_x(float f){
        x=f;
    }
    void set_y(float f){
        y=f;
    }
    void set_z(float f){
        z=f;
    }

    void print() {

        std::cout << "("<< x << "," << y <<  "," << z << ")" << std::endl;
    }

    // Destructor
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        //std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 operator+(const Vector3 &v,const Vector3 &v2){
    Vector3 c(v.xval()+v2.xval(),v.yval()+v2.yval(),v.zval()+v2.zval());
    return c;
}

Vector3 operator *(const Matrix4 &a, const Vector3 &v){
    Vector3 c(a.get_0()*v.xval()+a.get_3()*v.yval()+a.get_6()*v.zval(),
              a.get_1()*v.xval()+a.get_4()*v.yval()+a.get_7()*v.zval(),
              a.get_2()*v.xval()+a.get_5()*v.yval()+a.get_8()*v.zval());
    return c;

}
