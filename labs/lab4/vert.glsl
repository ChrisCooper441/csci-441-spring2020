#version 330 core
layout (location = 0) in vec4 aPos;
layout (location = 1) in vec3 aColor;

uniform mat4 transform;
uniform mat4 viewport;
uniform mat4 project;
out vec3 myColor;

void main() {
    gl_Position = project * viewport * transform * aPos;
    myColor = aColor;
}
