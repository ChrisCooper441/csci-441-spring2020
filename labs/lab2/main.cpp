#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(640, 480, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    /** YOU WILL ADD CODE STARTING HERE */

    GLuint shader = 0;
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader
    shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src_ptr, NULL);
    glCompileShader(shader);

    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
                  <<"::COMPILATION_FAILED\n"
                  << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    GLuint program = 0;
    program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
                  << infoLog << std::endl;
    }

    return program;
}



//Class for storing both position and color information for each Vertex of the triangle
class Vertex{
public:
    float x, y, r, g, b;

    // Constructor
    Vertex(float xx, float yy, float rr, float gg, float bb) : x(xx), y(yy), r(rr), g(gg), b(bb) {
        //cout << "Construct Vertex" << endl;
    }

    // Default constructor
    Vertex() : x(0), y(0), r(0), g(0), b(0) {
        //cout << "Construct Default Vertex" << endl;
    }

    ~Vertex() {
        // cout << "in Vertex destructor" << endl;
    }

    void SetVertex(float xx, float yy, float rr, float gg, float bb) {
        x = xx;
        y = yy;
        r = rr;
        g = gg;
        b = bb;
    }
};

//2D Vector for cartesian coordinates
class Vector2 {
public:
    float x;
    float y;

    //Constructors
    Vector2(float xx, float yy) : x(xx), y(yy) {}
    Vector2() : x(0), y(0) {}
    ~Vector2() {
        // this is where you would release resources such as memory or file descriptors
        // cout << "in Vector2 destructor" << endl;
    }

};

Vector2 w2nd(Vector2 vec, float width, float height) {
    float xnd = -1 + vec.x*2/width;
    float ynd = 1 - vec.y*2/height;

    return Vector2(xnd, ynd);
}

// Overrides stream output for data vertices
std::ostream& operator<<(std::ostream& stream, const Vertex& v) {
    stream << "Normalized vertex: (" << v.x << "," << v.y << ") has color: (" << v.r << "," << v.g << "," << v.b << ")";
    return stream;
}

int main(void) {
    GLFWwindow* window = initWindow();
    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }

    /** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */

    /* PART1: ask the user for coordinates and colors, and convert to normalized
     * device coordinates */
    // Statically set window size for now
    float width, height;
    width = 640;
    height = 480;

    Vertex vertices[3];
    std::string x, y, r, g, b;

    // Get user input
    std::cout << "Enter 3 Vertices (enter a point as x y r g b) " << std::endl;
    for(int i = 0; i < 3; i++) {
        // read in as string and convert to float
        std::cin >> x >> y >> r >> g >> b;

        vertices[i] = Vertex(stof(x), stof(y),stof(r),stof(g),stof(b));
    }

    //test inputs
//    vertices[0] =  Vertex( 0.84375f,  0.791667f, 1.0f, 0.0f, 0.0f );
//    vertices[1] =  Vertex(-0.875f, 0.916667f, 0.0f, 1.0f, 0.0f);
//    vertices[2] = Vertex( 0.0625f, -0.666667f, 0.0f, 0.0f, 1.0f) ;


//    // print out user input
    std::cout << "Normalized coordinates" << std::endl;
    for (int i = 0; i < 3; i++) {
        std::cout << vertices[i] << std::endl;
        vertices[i].x = w2nd(Vector2(vertices[i].x, vertices[i].y), width, height).x;
        vertices[i].y = w2nd(Vector2(vertices[i].x, vertices[i].y), width, height).y;
        std::cout << vertices[i] << std::endl;
    }

    // GLfloat triangle[] = (researched but not used)
    // convert the triangle to an array of floats containing
    // normalized device coordinates and color components.

    float triangle[] =
            { vertices[0].x, vertices[0].y, 0.0f, vertices[0].r, vertices[0].g, vertices[0].b,
              vertices[1].x, vertices[1].y, 0.0f, vertices[1].r, vertices[1].g, vertices[1].b,
              vertices[2].x, vertices[2].y, 0.0f, vertices[2].r, vertices[2].g, vertices[2].b
            } ;

    // PART2: map the data

    // create vertex and array buffer objects using
    // glGenBuffers, glGenVertexArrays
    GLuint VBO[1], VAO[1];
    glGenBuffers(1, VBO);
    glGenVertexArrays(1, VAO);

    // setup triangle using glBindVertexArray, glBindBuffer, GlBufferData
    glBindVertexArray(*VAO);
    glBindBuffer(GL_ARRAY_BUFFER, *VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // setup the attribute pointer for the coordinates
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // setup the attribute pointer for the colors
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3* sizeof(float)));
    glEnableVertexAttribArray(1);

    /** PART3: create the shader program */

    // create the shaders
    // YOU WILL HAVE TO ADD CODE TO THE createShader FUNCTION ABOVE
    GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);

    // create the shader program
    // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

    // cleanup the vertex and fragment shaders using glDeleteShader
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);


    /** END INITIALIZATION CODE */

    while (!glfwWindowShouldClose(window)) {
        // you don't need to worry about processInput, all it does is listen
        // for the escape character and terminate when escape is pressed.
        processInput(window);

        /** YOU WILL ADD RENDERING CODE STARTING HERE */
        /** PART4: Implementing the rendering loop */

        // clear the screen with your favorite color using glClearColor devil's gray
        glClearColor(0.6f, 0.6f, 0.6f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // set the shader program using glUseProgram
        glUseProgram(shaderProgram);
        // bind the vertex array using glBindVertexArray
        glBindVertexArray(*VAO);
        // draw the triangles using glDrawArrays
        glDrawArrays(GL_TRIANGLES, 0, 3);

        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
