//
// Matrix file created for better manipulation of matrices
// Collaborated with Jarey Boyer
//


#ifndef LAB3_MATRIX3_H
#define LAB3_MATRIX3_H
#endif //LAB3_MATRIX3_H
#include <cmath>
#include <iostream>
#include <GLFW/glfw3.h>

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <array>


class Matrix3 {
public:
    float data[9];

    Matrix3(){
        data[0] = 1;
        data[1] = 0;
        data[2] = 0;
        data[3] = 0;
        data[4] = 1;
        data[5] = 0;
        data[6] = 0;
        data[7] = 0;
        data[8] = 1;

    }

    Matrix3(float inputData[9]){
        data[0] = inputData[0];
        data[1] = inputData[1];
        data[2] = inputData[2];
        data[3] = inputData[3];
        data[4] = inputData[4];
        data[5] = inputData[5];
        data[6] = inputData[6];
        data[7] = inputData[7];
        data[8] = inputData[8];

    }

    // getters
    float zero() const {return data[0];}
    float one() const {return data[1];}
    float two() const {return data[2];}
    float three() const {return data[3];}
    float four() const {return data[4];}
    float five() const {return data[5];}
    float six() const {return data[6];}
    float seven() const {return data[7];}
    float eight() const {return data[8];}

    // setters
    void set0(float f){data[0]=f;}
    void set1(float f){data[1]=f;}
    void set2(float f){data[2]=f;}
    void set3(float f){data[3]=f;}
    void set4(float f){data[4]=f;}
    void set5(float f){data[5]=f;}
    void set6(float f){data[6]=f;}
    void set7(float f){data[7]=f;}
    void set8(float f){data[8]=f;}

    float* toArray() {
        return &data[0];
    }

    void print() const {

        std::cout <<"("<< data[0] << "," << data[3] << ","<< data[6]<<")" << std::endl;
        std::cout <<"("<< data[1] << "," << data[4] << ","<< data[7]<<")" << std::endl;
        std::cout <<"("<< data[2] << "," << data[5] << ","<< data[8]<<")" << std::endl;
    }
};

Matrix3 operator *(const Matrix3 &a, const Matrix3 &b){

    float z[9];

    Matrix3 c=z;
    c.set0(a.zero()*b.zero()+a.three()*b.one()+a.six()*b.two());
    c.set1(a.one()*b.zero()+a.four()*b.one()+a.seven()*b.two());
    c.set2(a.two()*b.zero()+a.five()*b.one()+a.eight()*b.two());

    c.set3(a.zero()*b.three()+a.three()*b.four()+a.six()*b.five());
    c.set4(a.one()*b.three()+a.four()*b.four()+a.seven()*b.five());
    c.set5(a.two()*b.three()+a.five()*b.four()+a.eight()*b.five());

    c.set6(a.zero()*b.six()+a.three()*b.seven()+a.six()*b.eight());
    c.set7(a.one()*b.six()+a.four()*b.seven()+a.seven()*b.eight());
    c.set8(a.two()*b.six()+a.five()*b.seven()+a.eight()*b.eight());


    return c;
}

Matrix3 identity(Matrix3 inputMatrix){
    Matrix3 identity;
    identity.set0(1);
    identity.set1(0);
    identity.set2(0);
    identity.set3(0);
    identity.set4(1);
    identity.set5(0);
    identity.set6(0);
    identity.set7(0);
    identity.set8(1);

    return identity * inputMatrix;
}

Matrix3 rotate(Matrix3 inputMatrix, float theta) {
    Matrix3 rotationMatrix;
    rotationMatrix.set0(cos(theta - 3));
    rotationMatrix.set1(sin(theta - 3));
    rotationMatrix.set2(0);
    rotationMatrix.set3(-sin(theta - 3));
    rotationMatrix.set4(cos(theta - 3));
    rotationMatrix.set5(0);
    rotationMatrix.set6(0);
    rotationMatrix.set7(0);
    rotationMatrix.set8(1);

    return rotationMatrix * inputMatrix;
}

Matrix3 scale(Matrix3 inputMatrix, float scalar) {
    Matrix3 scaleMatrix;
    float scaleMult = .2;
    scaleMatrix.set0(scaleMult*scalar/5);
    scaleMatrix.set1(0);
    scaleMatrix.set2(0);
    scaleMatrix.set3(0);
    scaleMatrix.set4(scaleMult*scalar/5);
    scaleMatrix.set5(0);
    scaleMatrix.set6(0);
    scaleMatrix.set7(0);
    scaleMatrix.set8(1);

    return  scaleMatrix * inputMatrix;
}




// reused Vector3 from lab1
class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx=0, float yy=0, float zz=0){ x=xx; y=yy; z=zz;
        // nothing to do here as we've already initialized x, y, and z above
        //std::cout << "in Vector3 constructor" << std::endl;
    }
    float xval() const {return x;}
    float yval() const {return y;}
    float zval() const {return z;}


    void set_x(float f){
        x=f;
    }
    void set_y(float f){
        y=f;
    }
    void set_z(float f){
        z=f;
    }

    void print() {

        std::cout << "("<< x << "," << y <<  "," << z << ")" << std::endl;
    }

    // Destructor
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        //std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 operator+(const Vector3 &v,const Vector3 &v2){
    Vector3 c(v.xval()+v2.xval(),v.yval()+v2.yval(),v.zval()+v2.zval());
    return c;
}

Vector3 operator *(const Matrix3 &a, const Vector3 &v){
    Vector3 c(a.zero()*v.xval()+a.three()*v.yval()+a.six()*v.zval(),
              a.one()*v.xval()+a.four()*v.yval()+a.seven()*v.zval(),
              a.two()*v.xval()+a.five()*v.yval()+a.eight()*v.zval());
    return c;

}
