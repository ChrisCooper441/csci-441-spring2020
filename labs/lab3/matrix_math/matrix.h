//
// Helper file to facilitate matrix operations
//

#ifndef LAB3_MATRIX_H
#define LAB3_MATRIX_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <csci441/shader.h>
#include <vector>

// required some research for using std::vector in matrices
// https://stackoverflow.com/questions/12375591/vector-of-vectors-to-create-matrix
// https://stackoverflow.com/questions/6775643/allocating-vectors-or-vectors-of-vectors-dynamically
typedef std::vector<std::vector<float>> Matrix;
Matrix operator+(Matrix& s1, Matrix& s2);
Matrix operator*(Matrix& s1, Matrix& s2);
std::ostream& operator<<(std::ostream& stream, Matrix &input);
Matrix translate(Matrix inputMatrix, Matrix inputVector);
Matrix scale(Matrix inputMatrix, float scalar);
Matrix rotate(Matrix inputMatrix, float degrees);
float* toArray(Matrix &inputMatrix);

#endif //LAB3_MATRIX_H

