//
// Helper file to facilitate matrix operations
//

#include "matrix.h"
#include <iostream>


Matrix operator+(Matrix& s1, Matrix& s2)
{
    //rows: sx.size() columns: sx[0].size()
    Matrix sum(s1.size(), std::vector<float>(s2[0].size(), 0));
    if (s1.size() != s2.size() || s1[0].size() != s2[0].size()) {
        std::cout << "Can't add matrices:\n"
                     "Matrices must be of the same size";
    }
    else {
        for (int i = 0; i < s1.size(); i++)
        {
            for (int j = 0; j < s2.size(); j++)
            {
                sum[i][j] += s1[i][j] + s2[i][j];
            }
        }
    }
    return sum;
}


//
//Matrix convert_to_col_vectors(Matrix& m){
//    int const m_rows = m.size(),
//            m_cols = m[0].size();
//    Matrix conv_matrix(m_cols + 1, std::vector<float>(m_rows, 0));
//    for (int i = 0; i < m_cols+1; i++)
//    {
//        for (int j = 0; j < m_rows; j++)
//        {
//            if (i == m_cols){
//                conv_matrix[i][j] = 1;
//            }else{
//                conv_matrix[i][j] += m[j][i];
//            }
//        }
//    }
//    std::cout << "CONV FOR TRANS" << std::endl;
//    std::cout << conv_matrix;
//    return conv_matrix;
//}

// override * and multiply both factors
Matrix operator* (Matrix& f1, Matrix& f2)
{
    Matrix product(f1.size(), std::vector<float>(f2[0].size(), 0));

    if (f1[0].size() != f2.size())
    {
        std::cout << "Can't multiply matrices:\n"
                     "The number of columns in the first matrix should be equal to the number of rows in the second";
    }

    // Matrix math help for multiplication https://www.programiz.com/cpp-programming/examples/matrix-multiplication
    else
    {
        for (int i = 0; i < f1.size(); i++)
        {
            for (int j = 0; j < f2[0].size(); j++)
            {
                for (int k = 0; k < f1[0].size(); k++)
                {
                    product[i][j] += f1[i][k] * f2[k][j];
                }
            }
        }
    }
    return product;
}


std::ostream& operator<<(std::ostream& stream, Matrix &input) {
    int rows = input.size();
    int cols = input[0].size();
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            std::cout << input[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

//http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
Matrix translate(Matrix inputMatrix, Matrix inputVector){
    // just using 3x1 matrix for vector
    Matrix translated = inputMatrix * inputVector;

    return translated;
}

Matrix scale(Matrix inputMatrix, float scalar){
    for (int i = 0; i < inputMatrix.size(); i++) {
        for (int j = 0; j < inputMatrix[0].size(); j++) {
            if(i == j) {
                inputMatrix[i][j] = inputMatrix[i][j] * scalar;
            }
        }
    }
}

Matrix rotate(Matrix inputMatrix, float theta){

}
//Matrix identity(Matrix inputMatrix, float theta){
//
//}

float* toArray(Matrix &inputMatrix){
    float output[9];
    int rows = inputMatrix.size();
    int cols = inputMatrix[0].size();
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            output[i*3+j] = inputMatrix[i][j];
        }
    }
    return output;
}