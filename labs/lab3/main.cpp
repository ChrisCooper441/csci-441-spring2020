//
// Chris Cooper
// Collaborated with Jarey Boyer for getting window buffer to work
//

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "matrix_math/matrix.h"
#include "matrix_math/Matrix3.h"

int MODE = 0;


void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){

    if(key == GLFW_KEY_SPACE && action == GLFW_RELEASE){

        if (MODE != 4){
            MODE ++;
        }
        else{
            MODE = 0;
        }
    }
}

int main(void) {
    Matrix data_in = { {1,2,10},
                       {4,5,6},
                       {7,8,1} };
    Matrix data_2 = { {1,2,3},
                       {4,5,6} };
    Matrix n( 3,std::vector<float>(3,1));
    //float point3[3][3] = {1, 2, 3, 4, 5, 6, 7, 8, 9 };

    //data_in = data_in + n;
    Matrix trans_vec = { {8},
                         {9},
                         {1} };
    Matrix translated = translate(data_in, trans_vec);
    std::cout << translated;
//    printMatrix(data_in);
    std::cout << data_in;







    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback(window, keyCallback);

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };
//    Matrix identity = { {1,0,0},
//                        {0,1,0},
//                        {0,0,1} };

//    std::cout << identity;

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_DYNAMIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);


    Matrix3 transformation;
    float angle = 0.005;
    Matrix3 drawData;
    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);
        float time = glfwGetTime();

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        Matrix3 transform=drawData;

        // use the shader


        switch(MODE){
            case 0 :
                transform = identity(drawData);
                break;
            case 1:
                transform = rotate(drawData, angle);   // implement angle
                angle += 0.005;
                break;
            case 2:
                transform = scale(drawData, time);
                break;
            case 3:
                transform = rotate(scale(drawData, time), angle);
        }

        shader.use();
        GLint uniformLocation = glGetUniformLocation(shader.id(), "transform");
        glUniformMatrix3fv(uniformLocation, 1, GL_TRUE, transform.toArray()); // need to get array
        /** Part 2 animate and scene by updating the transformation matrix */




        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }
    glDeleteShader(shader.id());
    glfwTerminate();
    return 0;
}
