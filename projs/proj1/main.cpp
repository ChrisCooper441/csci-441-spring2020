#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix4.h>
#include <csci441/matrix3.h>
#include <csci441/vector4.h>
#include <csci441/uniform.h>

#include "model.h"
#include "camera.h"
#include "renderer.h"
#include "mesh.h"

void framebufferSizeCallback(GLFWwindow* window, int width, int height);
bool isPressed(GLFWwindow *window, int key);
bool isReleased(GLFWwindow *window, int key);
void processInput(GLFWwindow *window);
void errorCallback(int error, const char* description);
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void processFpsMovement(GLFWwindow* window);
void processTopMovement(GLFWwindow* window);

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 800;

// camera
Camera fpsCamera;
Camera topCamera;
float lastX = SCREEN_WIDTH / 2.0f;
float lastY = SCREEN_HEIGHT / 2.0f;
bool fps = true;
bool mousePressed = false;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback(window, keyCallback);

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    Model maze_floor(
        Mesh("../models/maze_floor.obj", true, 0, 0, 0),
        Shader("../vert.glsl", "../frag.glsl")
    );

    Model maze_walls(
        Mesh("../models/maze_walls.obj", true, 1, 0, 0),
        Shader("../vert.glsl", "../frag.glsl")
    );

    Model dino(
        Mesh("../models/dino.obj", false, 0, 0, 1),
        Shader("../vert.glsl", "../frag.glsl")
    );

    // setup camera projections
    Matrix4 fpsProjection;
    fpsProjection.perspective(45, 1, .01, 100);
    Matrix4 topProjection;
    topProjection.ortho(-40, 40, -40, 40, .01, 100);

    // setup fps camera
    fpsCamera.projection = fpsProjection;
    fpsCamera.eye = Vector4(5, 2, -31.5);
    fpsCamera.origin = Vector4(5, 2, 0);
    fpsCamera.up = Vector4(0, 1, 0);
    fpsCamera.view = fpsCamera.look_at();
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);

    // setup top camera
    topCamera.projection = topProjection;
    topCamera.eye = Vector4(0, 40, 0);
    topCamera.origin = Vector4(0, 0, 0);
    topCamera.up = Vector4(0, 0, 1);
    topCamera.view = topCamera.look_at();

    // setup character
    Matrix4 rotateDino;
    Matrix4 scaleDino;
    rotateDino.rotate_y(-90);
    scaleDino.scale(5, 5, 5);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector4 lightPos(20.0f, 20.0f, 20.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {

        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the object and the floor
        if (fps) {
            renderer.render(fpsCamera, maze_floor, lightPos);
            renderer.render(fpsCamera, maze_walls, lightPos);
        } else {
            Vector4 position = fpsCamera.getPosition();
            Matrix4 translateDino;
            translateDino.translate(position.x(), position.y(), position.z());
            dino.model = translateDino * scaleDino * rotateDino;
            renderer.render(topCamera, maze_floor, lightPos);
            renderer.render(topCamera, maze_walls, lightPos);
            renderer.render(topCamera, dino, lightPos);
        }

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

void processInput(GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE)) {
        glfwSetWindowShouldClose(window, true);
    }
    if (fps) processFpsMovement(window);
    else processTopMovement(window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
        fps = !fps;
    }
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    if (!mousePressed) {
        lastX = xpos;
        lastY = ypos;
        return;
    }

    float xoffset = xpos - lastX;
    float yoffset = ypos - lastY; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    fpsCamera.processMouseMovement(xoffset, yoffset);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        if (action == GLFW_PRESS) mousePressed = true;
        else if (action == GLFW_RELEASE) mousePressed = false;
    }
}

void processFpsMovement(GLFWwindow* window) {
    float dx = 0;
    float dz = 0;
    if (isPressed(window, GLFW_KEY_W)) {
        dz = 2;
    } else if (isPressed(window, GLFW_KEY_S)) {
        dz = -2;
    } else if (isPressed(window, GLFW_KEY_A)) {
        dx = -2;
    } else if (isPressed(window, GLFW_KEY_D)) {
        dx = 2;
    }
    if (dx != 0 || dz != 0) {
        fpsCamera.strafe(dx, dz);
    }
}

void processTopMovement(GLFWwindow* window) {
    float dx = 0;
    float dz = 0;
    if (isPressed(window, GLFW_KEY_UP)) {
        dz = 1;
    } else if (isPressed(window, GLFW_KEY_DOWN)) {
        dz = -1;
    } else if (isPressed(window, GLFW_KEY_LEFT)) {
        dx = 1;
    } else if (isPressed(window, GLFW_KEY_RIGHT)) {
        dx = -1;
    }
    if (dx != 0 || dz != 0) {
        fpsCamera.processEyeMovement(dx, dz);
    }
}