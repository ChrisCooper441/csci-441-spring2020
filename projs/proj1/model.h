#ifndef _CSCI441_MODEL_H_
#define _CSCI441_MODEL_H_

#include "mesh.h"

class Model {

public:
    Mesh mesh;
    GLuint ebo;
    GLuint vbo;
    GLuint vao;
    Shader shader;
    Matrix4 model;
    int size;

    Model(Mesh mesh, const Shader& shader_in) : mesh(mesh), shader(shader_in) {
        std::vector<float> coords = mesh.getBufferData();
        std::vector<unsigned int> elements = mesh.getElements();
        size = elements.size();

        // gen buffers
        glGenVertexArrays(1, &vao);
        glGenBuffers(1, &vbo);
        glGenBuffers(1, &ebo);

        // flat shading
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, coords.size()*sizeof(float), &coords[0], GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size()*sizeof(unsigned int), &elements[0], GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(0*sizeof(float)));
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9*sizeof(float),
                (void*)(6*sizeof(float)));
        glEnableVertexAttribArray(2);
    }
};

#endif
