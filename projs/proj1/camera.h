#ifndef _CSCI441_CAMERA_H_
#define _CSCI441_CAMERA_H_

#include <csci441/matrix4.h>
#include <csci441/vector4.h>

class Camera {
private:
    float pitch;
    float yaw;
    Matrix4 matPitch;
    Matrix4 matYaw;
    Matrix4 rotate;
    Matrix4 translate;
public:
    Matrix4 projection;
    Vector4 eye;
    Vector4 origin;
    Vector4 up;
    Matrix4 view;

    Camera() : eye(0,0,0), origin(0,0,0), up(0,0,0), pitch(0), yaw(180) {}

    Matrix4 look_at() const {
        Matrix4 mat;
        mat.look_at(eye, origin, up);
        return mat;
    }

    void strafe(float dx, float dz) {
        const float speed = 0.01f;
        Vector4 forward = Vector4(view.get(2, 0), 0, view.get(2, 2));
        Vector4 strafe = Vector4(view.get(0, 0), 0, view.get(0, 2));
        Vector4 delta = (forward.scale(-dz) + strafe.scale(dx)).scale(speed);
        eye = eye + delta;
        updateView();
    }

    void processEyeMovement(float dx, float dz) {
        const float sensX = 0.05;
        const float sensZ = 0.05;
        Vector4 delta(dx * sensX, 0, dz * sensZ);
        eye = eye + delta;
        updateView();
    }

    void processMouseMovement(float dx, float dy) {
        const float sensX = 0.25f;
        const float sensY = 0.25f;
        pitch += sensX * dy;
        yaw += sensY * dx;
        updateView();
    }

    void updateView() {
        matPitch.rotate_x(pitch);
        matYaw.rotate_y(yaw);
        rotate = matPitch * matYaw;
        translate.translate(-eye.x(), -eye.y(), -eye.z());
        view = rotate * translate;
    }

    Matrix4 getViewMatrix() const {
        return view;
    }

    Vector4 getPosition() {
        return eye;
    }

};

#endif
