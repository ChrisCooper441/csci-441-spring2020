#ifndef _CSCI441_VECTOR3_H_
#define _CSCI441_VECTOR3_H_

#include <cmath>
#include <iostream>


class Vector3 {
public:
    float x, y, z;
    Vector3() {}
    Vector3(float x, float y, float z): x(x), y(y), z(z) {}
    Vector3 cross(Vector3 &v) {
        float i, j, k;
        i = y * v.z - z * v.y;
        j = -(x * v.z - z * v.x);
        k = x * v.y - y * v.x;
        return Vector3(i, j, k);
    }
    Vector3 normalize() {
        float length = sqrtf(powf(x, 2) + powf(y, 2) + powf(z, 2));
        return Vector3(x/length, y/length, z/length);
    }
    Vector3 operator-() {
        return Vector3(-x, -y, -z);
    }
    Vector3 operator-(Vector3 &v) {
        return Vector3(x-v.x, y-v.y, z-v.z);
    }
    float operator*(Vector3 &v) {
        return x*v.x + y*v.y + z*v.z;
    }
};

#endif