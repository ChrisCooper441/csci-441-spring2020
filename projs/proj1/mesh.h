#ifndef _CSCI441_MESH_H_
#define _CSCI441_MESH_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

class Vector3 {
public:
    float x, y, z;
    Vector3() {}
    Vector3(float x, float y, float z): x(x), y(y), z(z) {}
    Vector3 cross(Vector3 &v) {
        float i, j, k;
        i = y * v.z - z * v.y;
        j = -(x * v.z - z * v.x);
        k = x * v.y - y * v.x;
        return Vector3(i, j, k);
    }
    Vector3 normalize() {
        float length = sqrtf(powf(x, 2) + powf(y, 2) + powf(z, 2));
        return Vector3(x/length, y/length, z/length);
    }
    Vector3 operator-() {
        return Vector3(-x, -y, -z);
    }
    Vector3 operator-(Vector3 &v) {
        return Vector3(x-v.x, y-v.y, z-v.z);
    }
};

class Vertex {
public:
    Vector3 coords;
    Vector3 norm;
    Vertex() {}
    Vertex(Vector3& coords): coords(coords) {}
};

class Face {
public:
    Vertex v1, v2, v3;
    unsigned int i, j, k;
    Face() {}
    Face(Vertex& v1, Vertex& v2, Vertex& v3, unsigned int& i, unsigned int& j, unsigned int& k): v1(v1), v2(v2), v3(v3), i(i), j(j), k(k) {}
};

class Mesh {

private:
    std::vector<Vertex> vertices;
    std::vector<Face> faces;
    std::vector<unsigned int> elements;
    float r, g, b;

    void calculateNorms() {
        std::vector<float> normals(vertices.size()*3, 0);
        std::vector<int> counts(vertices.size(), 0);
        Face face;
        Vector3 normal, edge1, edge2;
        for (int i = 0; i < faces.size(); i++) {
            face = faces[i];
            // increment count of each vertex
            counts[face.i]++;
            counts[face.j]++;
            counts[face.k]++;
            // calculate edges
            edge1 = face.v2.coords - face.v1.coords;
            edge2 = face.v3.coords - face.v1.coords;
            // calculate normal between two edges
            normal = edge1.cross(edge2).normalize();
            
            normals[face.i*3] += normal.x;
            normals[face.j*3] += normal.x;
            normals[face.k*3] += normal.x;
            normals[face.i*3+1] += normal.y;
            normals[face.j*3+1] += normal.y;
            normals[face.k*3+1] += normal.y;
            normals[face.i*3+2] += normal.z;
            normals[face.j*3+2] += normal.z;
            normals[face.k*3+2] += normal.z;
        }
        int count;
        for (int i = 0; i < vertices.size(); i++) {
            count = counts[i];
            vertices[i].norm = Vector3(normals[i*3]/count, normals[i*3+1]/count, normals[i*3+2]/count);
        }
    }

public:
    Mesh(const std::string& path, bool flipyz, float r, float g, float b): r(r), g(g), b(b) {
        // read in vertices and faces from .obj file
        std::ifstream file;
        std::string line;
        char c;
        float x, y, z;
        unsigned int i, j, k;
        file.open(path);
        while(std::getline(file, line)) {
            std::istringstream iss(line);
            iss >> c;
            if (c == 'v') {
                iss >> x >> y >> z;
                if (flipyz) {
                    Vector3 coords(x,z,y);
                    vertices.push_back(Vertex(coords));
                } else {
                    Vector3 coords(x,y,z);
                    vertices.push_back(Vertex(coords));
                }
            } else if (c == 'f') {
                iss >> i >> j >> k;
                i--; j--; k--;
                faces.push_back(Face(vertices[i],vertices[j],vertices[k],i,j,k));
                elements.push_back(i);
                elements.push_back(j);
                elements.push_back(k);
            }
        }
    }

    std::vector<float> getBufferData() {
        calculateNorms();
        std::vector<float> data;
        Vertex v;
        for (int i = 0; i < vertices.size(); i++) {
            v = vertices[i];
            // location
            data.push_back(v.coords.x);
            data.push_back(v.coords.y);
            data.push_back(v.coords.z);
            // color
            data.push_back(r);
            data.push_back(g);
            data.push_back(b);
            // normal
            data.push_back(v.norm.x);
            data.push_back(v.norm.y);
            data.push_back(v.norm.z);
        }
        return data;
    }

    std::vector<unsigned int> getElements() {
        return elements;
    }

    int size() {
        return elements.size();
    }

};

#endif