#ifndef _CSCI441_RENDERER_H_
#define _CSCI441_RENDERER_H_

#include <csci441/matrix3.h>

class Renderer {
    Matrix3 itModel_tmp;
public:

    void render(const Camera& camera, Model& m, const Vector4& light) {
        itModel_tmp.inverse_transpose(m.model);

        m.shader.use();
        Uniform::set(m.shader.id(), "model", m.model);
        Uniform::set(m.shader.id(), "projection", camera.projection);
        Uniform::set(m.shader.id(), "camera", camera.getViewMatrix());
        Uniform::set(m.shader.id(), "eye", camera.eye);
        Uniform::set(m.shader.id(), "lightPos", light);
        Uniform::set(m.shader.id(), "itModel", itModel_tmp);

        // render the cube
        glBindVertexArray(m.vao);
        glDrawElements(GL_TRIANGLES, m.size, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }
};

#endif
